// The Vue build version to load with the `import` command
import Vue from 'vue'
import App from './App'
import './utils/rem.js';
import router from './router'
import default_img from '@/assets/images/default.png'
import Title from 'vue-wechat-title'
import Axios from 'axios';

//懒加载默认图
let options = {
  loading: default_img,
  error: default_img
};

//vant组件
import { Lazyload } from 'vant';



Vue.config.productionTip = false;
Vue.prototype.$axios = Axios;
Vue.use(Title).use(Lazyload, options);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {App},
  template: '<App/>'
})
